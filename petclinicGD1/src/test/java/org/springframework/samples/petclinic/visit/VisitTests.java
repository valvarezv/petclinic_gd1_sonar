/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.Month;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class VisitTests {
	
	public static Visit visit;
	public static Vet vet, vet2;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		visit = new Visit();
		LocalDate visityear = LocalDate.of(2018, Month.JANUARY, 26);
		visit.setDate(visityear);
		visit.setDescription("visita planificada");
		visit.setPetId(3);
		
		vet = new Vet();
		vet.setFirstName("Manuel");
		vet.setLastName("Nieto");
		vet.setHomeVisits(false);
	}
	
	@Before
	//This test is executed before each test created in this suite
	public void init() {
		visit.setVet(vet);
	}
	
	@Test
	public void testGetVet() {
		assertNotNull(visit.getVet());
		assertEquals(visit.getVet().getFirstName(), "Manuel");
		assertEquals(visit.getVet().getLastName(), "Nieto");
		assertFalse(visit.getVet().getHomeVisits());
	}
	
	@Test
	public void testSetVet() {
		assertNotNull(visit.getVet());
		assertEquals(visit.getVet().getFirstName(), "Manuel");
		assertEquals(visit.getVet().getLastName(), "Nieto");
		assertFalse(visit.getVet().getHomeVisits());
	
		vet2 = new Vet ();
		vet2.setFirstName("Victor");
		vet2.setLastName("Alvarez");
		vet2.setHomeVisits(true);
		visit.setVet(vet2);
		
		assertNotNull(visit.getVet());
		assertEquals(visit.getVet().getFirstName(), "Victor");
		assertEquals(visit.getVet().getLastName(), "Alvarez");
		assertTrue(visit.getVet().getHomeVisits());
		
		
	}
    
    @After
    //This test is executed after each test created in this suite
    public void finish() {
    	visit.setVet(null);
    }
    
    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	vet = null;
    	vet2 = null;
		visit = null;
    }
    

}
