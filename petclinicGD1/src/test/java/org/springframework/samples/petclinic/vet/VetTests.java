/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.Month;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;
public class VetTests {

	public static Vet vet;
	
	public static Vet vet2;
	
	
	@Before
	//This test is executed before each test created in this suite
	public void init() {
		vet = new Vet();
		vet.setFirstName("Juan Manuel");
		vet.setLastName("Murillo");
		vet.setHomeVisits(false);
	}
	
	
	@Test
	public void testGetVisits() {
		assertNotNull(vet.getVisits());
		assertTrue(vet.getVisits().isEmpty());
		
		Visit v=new Visit();
		LocalDate date=LocalDate.of(2017, Month.APRIL, 23);
		v.setDate(date);
		v.setDescription("Urgente");
		vet.addVisit(v);
		
		assertTrue(vet.getVisits().size()==1);
	}
	
	
	@Test
	public void testAddVisit() {
		assertTrue(vet.getVisits().size()==0);
		
		Visit v=new Visit();
		LocalDate date=LocalDate.of(2017, Month.APRIL, 23);
		v.setDate(date);
		v.setDescription("Urgente");
		vet.addVisit(v);
		assertTrue(vet.getVisits().size()==1);
		
	}
	
	
	@Test
	public void testVet() {
		assertNotNull(vet);				
	}
	
	@Test
	public void testGetHomeVisits() {
		assertNotNull(vet.getHomeVisits());
		assertFalse(vet.getHomeVisits());
	}
	
	@Test
	public void testSetHomeVisits() {
		assertNotNull(vet.getHomeVisits());
		assertFalse(vet.getHomeVisits());
		vet.setHomeVisits(true);
		assertTrue(vet.getHomeVisits());
	}
	
    @Test
    public void testSerialization() {
        Vet vet = new Vet();
        vet.setFirstName("Zaphod");
        vet.setLastName("Beeblebrox");
        vet.setId(123);
        Vet other = (Vet) SerializationUtils
                .deserialize(SerializationUtils.serialize(vet));
        assertThat(other.getFirstName()).isEqualTo(vet.getFirstName());
        assertThat(other.getLastName()).isEqualTo(vet.getLastName());
        assertThat(other.getId()).isEqualTo(vet.getId());
    }
    
  
	
	
	@Ignore
	//This test is not executed
	public void ignore() {
		vet.setHomeVisits(true);
	}
    
    @After
    //This test is executed after each test created in this suite
    public void finish() {
    	vet.setHomeVisits(true);
    }
    
    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	vet = null;
    }
    

}
